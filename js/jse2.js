    // Función generar calificaciones
    function generarCalificaciones() {
        const calificaciones = [];
        for (let i = 0; i < 35; i++) {
            calificaciones.push(Math.floor(Math.random() * 11));
        }
        return calificaciones;
    }
    
    function mostrarCalificaciones(calificaciones) {
        return "Calificaciones de los alumnos: <br> <br> <hr>" + calificaciones.join(" <hr>");
    }
    
    //   Función calcular
    function calcularEstadisticas() {
        const calificaciones = generarCalificaciones();
        const aprobados = calificaciones.filter(calificacion => calificacion >= 7);
        const noAprobados = calificaciones.filter(calificacion => calificacion < 7);
        const promedioNoAprobados = calcularPromedio(noAprobados);
        const promedioAprobados = calcularPromedio(aprobados);
        const promedioGeneral = calcularPromedio(calificaciones);
    

        // Función mostrar
        const resultado = `
        ${mostrarCalificaciones(calificaciones)}<br>
        <hr><br><br>Alumnos aprobados: ${aprobados.length}<br>
        Alumnos no aprobados: ${noAprobados.length}<br>
        Calificación promedio de no aprobados: ${promedioNoAprobados.toFixed(2)}<br>
        Calificación promedio de aprobados: ${promedioAprobados.toFixed(2)}<br>
        Promedio General: ${promedioGeneral.toFixed(2)}
        `;
    
        document.getElementById("resultado").innerHTML = resultado;
    }

    //   Función para calcular promedio
    function calcularPromedio(calificaciones) {
        if (calificaciones.length === 0) {
        return 0;
        }
        const suma = calificaciones.reduce((total, calificacion) => total + calificacion, 0);
        return suma / calificaciones.length;
    }
    