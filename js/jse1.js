document.getElementById("calcularBtn").addEventListener("click", function() {
    calcularFrecuenciaCardiaca();
});

function calcularFrecuenciaCardiaca() {
    // Obtenemos la edad ingresada por el usuario
    var edad = parseFloat(document.getElementById("edadInput").value);

    // Definimos los rangos de frecuencia cardíaca según la edad
    var rangoFrecuencia = "";

    if (edad >= 0 && edad <= 1) {
        rangoFrecuencia = "140-160 pulsaciones por minuto";
    } else if (edad >= 2 && edad <= 12) {
        rangoFrecuencia = "110-115 pulsaciones por minuto";
    } else if (edad >= 13 && edad <= 40) {
        rangoFrecuencia = "70-80 pulsaciones por minuto";
    } else {
        rangoFrecuencia = "60-70 pulsaciones por minuto";
    }

    document.getElementById("resultado").innerHTML = "Tu rango de frecuencia cardíaca es de aproximadamente " + rangoFrecuencia + ".";
}
